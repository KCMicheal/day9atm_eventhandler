﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAO___Task_3.Models;

namespace BEZAO___Task_3.EntityConfigurations
{
    public class CustomerConfiguration: EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
          
                Property(c => c.FullName)
                .IsRequired()
                .HasMaxLength(255);

                HasMany(c => c.Transactions)
                .WithRequired(c => c.Customer)
                .HasForeignKey(c => c.CustomerId);


        
        }
    }
}
