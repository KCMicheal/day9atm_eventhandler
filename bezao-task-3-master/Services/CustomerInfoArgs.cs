﻿using System;
using BEZAO___Task_3.Helpers;
using BEZAO___Task_3.Models;

namespace BEZAO___Task_3.Services
{
    public class CustomerInfoArgs: EventArgs
    {
        public double Amount { get; set; }
        public Customer Customer { get; set; }
        public string Alert { get; set; }

        public TransactionType TransactionType { get; set; }
        public CustomerInfoArgs( Customer customer, double amount, string alert, TransactionType transactionType)
        {
            Amount = amount;
            Alert = alert;
            TransactionType = transactionType;
            Customer = customer;

        }
    }
}