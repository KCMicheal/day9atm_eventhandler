﻿using System;
using System.Text.RegularExpressions;
using BEZAO___Task_3.Helpers;
using BEZAO___Task_3.Interfaces;
using BEZAO___Task_3.Models;

namespace BEZAO___Task_3.Services
{
    public class Form
    {
        private static string _response;
        private static string _fname;
        private static string _lname;
        private static DateTime _dateOfBirth;
        private static long _accountNumber;
        private static double _amount;

        public static Customer CustomerEnrollment(ILogger logger, Func<string, string> prompt)
        {
           
            prompt = TextBuilder.Prompt;
            while (true)
            {
                logger.Log(prompt("Enter Your First Name:"));
                _response = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(_response))
                {
                    _fname = _response;
                    break;
                }
                logger.Log("First Name Field Cant be Blank!");
            }
            
            while (true)
            {
                logger.Log(prompt("Enter Your Last  Name:"));
                _response = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(_response))
                {
                    _lname = _response;
                    break;
                }
                logger.Log("Last Name Field Cant be Blank!");

            }
            
        
            while (true)
            {
                logger.Log(prompt("Enter Your Date of Birth in this Format dd/mm/yyyy:"));
                _response = Console.ReadLine();
                if (Regex.Match(_response ?? throw new InvalidOperationException(), @"^\d{1,2}\/\d{1,2}\/\d{4}$").Success)
                {
                    if (DateTime.TryParse(_response, out var dob))
                    { _dateOfBirth= dob;
                        break;
                    }

                    logger.Log("Date is Invalid!!!");
                }
                if (!Regex.Match(_response ?? throw new InvalidOperationException(), @"^\d{1,2}\/\d{1,2}\/\d{4}$").Success)
                {
                    logger.Log("Date is In a wrong Format!!!");
                }

            }

            Customer customer = new Customer(_fname, _lname, _dateOfBirth);

            return customer;

        }

        public static long Account(ILogger logger, Func<string, string> prompt)
        {
            prompt = TextBuilder.Prompt;
            while (true)
            {
                logger.Log(prompt("Enter Your Account Number:"));
                _response = Console.ReadLine();


                if (Regex.Match(_response ?? throw new InvalidOperationException(), @"^\d{9,10}$").Success)
                {
                    if (long.TryParse(_response, out var accountNumber))
                    {
                        _accountNumber = accountNumber;
                        break;
                    }
                }
                else
                {
                    logger.Log(" Invalid account Number!!!");
                }
            }

            return _accountNumber;
        }

        public static (long account, double amount) Transaction(ILogger logger, Func<string, string> prompt)
        {
            

            while (true)
            {
                prompt = TextBuilder.Prompt;
                logger.Log(prompt("Enter Amount:"));
                _response = Console.ReadLine();


                if (Regex.Match(_response ?? throw new InvalidOperationException(), @"^\d{3,30}(\.\d{2})?$").Success)
                {
                    if (double.TryParse(_response, out var amount))
                    {
                        _amount = amount;
                        break;
                    }
                }
                else
                {
                    logger.Log(" Invalid amount !!!");
                }
            }

            return (Account(logger, prompt), _amount);


        }
    }
}