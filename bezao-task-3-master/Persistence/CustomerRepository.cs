﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BEZAO___Task_3.Helpers;
using BEZAO___Task_3.Interfaces;
using BEZAO___Task_3.Models;
using BEZAO___Task_3.Services;

namespace BEZAO___Task_3.Persistence
{
    public class CustomerRepository
    {
        private readonly ILogger _logger;

        public EventHandler<CustomerInfoArgs> BankAlert;
        private readonly BankContext _dbContext = new BankContext();

        public CustomerRepository(ILogger logger)
        {
            _logger = logger;
        }

        public void Add(Customer customer)
        {
            _logger.Log("Please wait...");
            _dbContext.Customers.Add(customer);
            _dbContext.SaveChanges();
        }

        public Customer GetCustomer(long accountNumber)
        {
            _logger.Log("Please wait...");
            return _dbContext.Customers.Include(c => c.Transactions).SingleOrDefault(c => c.AccountNumber == accountNumber);
        }
        public IEnumerable<Customer> GetCustomers()
        {
            _logger.Log("Please wait...");
            return _dbContext.Customers.Include(c => c.Transactions).ToList().OrderByDescending(c => c.Balance);
        }
        public void CustomerDeposit(long account, double amount)
        {
            var customerBalance = GetCustomer(account);
            customerBalance.Balance += amount;
            _dbContext.SaveChanges();
            BankAlert?.Invoke(this, new CustomerInfoArgs(customerBalance, amount, "Credit",TransactionType.Credit));


        }

        public void CustomerWithdraw(long account, double amount)
        {
            var customerBalance = GetCustomer(account);
            if (customerBalance.Balance < amount)
            {
                _logger.Log("Insufficient Fund!!");
                return;
            }
            customerBalance.Balance -= amount;
            _dbContext.SaveChanges();
            BankAlert?.Invoke(this, new CustomerInfoArgs(customerBalance, amount, "Debit", TransactionType.Debit));

        }
    }
}