﻿using System;
using System.Data.Entity;
using BEZAO___Task_3.EntityConfigurations;
using BEZAO___Task_3.Models;

namespace BEZAO___Task_3.Persistence
{
    class BankContext : DbContext
    {

        public BankContext()
            : base("name=DefaultConnection")
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());

        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CustomerConfiguration());
            modelBuilder.Configurations.Add(new TransactionConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}