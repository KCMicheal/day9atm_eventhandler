﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notification
{

   public abstract class Notification
    {
        public virtual string Notify()
        {
            return string.Empty;
        }
    }

    public abstract class Sms : Notification
    {
        protected internal string Msg { get; set; }
        protected internal string PhoneNumber { get; set; }

        public Sms()
        {
            Msg = "Sending Sms";
            PhoneNumber = "07068040924";
        }

        public override string Notify()
        {
            return $"SMS, '{Msg}' was delivered to {PhoneNumber}";
        }
    }

    public abstract class Email : Notification
    {
        public string Msg { get; set; }
        public string EmailAddress { get; set; }

        public Email()
        {
            Msg = "Sending Email";
            EmailAddress = "user.name@gmail.com";
        }

        public override string Notify()
        {
            return $"Email, '{Msg} was sent to {EmailAddress}";
        }
    }

    public class WhatsAppNotification : Sms
    {
     
        public override string Notify()
        {
            return $"WhatsApp {Msg} was delivered to {PhoneNumber} ";
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            /*            var notification = new Notification();
                        var notify = notification.Notify();

                        var sms = new Sms();
                        var sendSms = sms.Notify();

                        var email = new Email();
                        var sendEmail = email.Notify();

                        Console.WriteLine($"notify: {notify}\n email: {sendEmail}\n sms: {sendSms}");*/


            int i = 1;
            object o = i;
            int j = (int)o;

            Console.WriteLine($"{o} {o.GetType()}");
            Console.ReadLine();

        }
    }
}
