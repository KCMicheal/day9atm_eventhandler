﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator1
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculator = new Calculator();
            var add1 = calculator.Add(1,2,3);
            var add2 = calculator.Add(1,2);

            Console.WriteLine($"{add1} and {add2}");

            Console.ReadLine();


        }
    }
}
